#!/bin/bash

### FUNCTIONS ###
exec_ansible () {
    curl -s https://gitlab.com/krohde/ftr-vl-ws-init/raw/master/$1 > $1
    ansible-playbook $1
}

#Install Ansible
curl -s https://gitlab.com/krohde/ftr-vl-ws-init/raw/master/01-ansible-install.sh > 01-ansible-install.sh
chmod +x 01-ansible-install.sh
./01-ansible-install.sh


exec_ansible 02-ansible-script.yaml 
exec_ansible 03-salesforce-cli.yaml
exec_ansible 04-install-vscode.yaml
exec_ansible 05-install-vlocity.yaml
exec_ansible 06-bash-aliases.yaml
#exec_ansible 10-postgre-install-ansible.yaml
exec_ansible 11-google-chrome.yaml

source ~/.bashrc

exit

#Configure local sources
#cp /etc/apt/sources.list.d/official-package-repositories.list /etc/apt/sources.list.d/official-package-repositories.list.bak
#sed -i 's|http://packages.linuxmint.com|http://mirrors.evowise.com/linuxmint/packages|g' /etc/apt/sources.list.d/official-package-repositories.list
#sed -i 's|http://archive.ubuntu.com|http://ftp.icm.edu.pl/pub/Linux|g' /etc/apt/sources.list.d/official-package-repositories.list

# Install VM tools first 
apt update && apt install open-vm-tools-desktop -y

# Upgrade distro to newest version
apt update && apt full-upgrade -y

# Install management software
apt update && apt install ansible -y

# Install Development tools

#GUI CONFIGURATION SETTINGS
# Configure terminal

#MANUAL STEPS
#1. Configure Timeshift

#!/bin/bash

#Install ansible
sudo apt-add-repository -y ppa:ansible/ansible
sudo apt-get update
sudo apt-get install -y ansible


#Configure
sudo mv /etc/ansible/hosts /etc/ansible/hosts.org

sudo bash -c 'echo "[local]
127.0.0.1 ansible_connection=local" > /etc/ansible/hosts'
